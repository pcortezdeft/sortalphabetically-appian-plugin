package com.appiancorp.plugins.newsort;

import java.util.Arrays;

import com.appiancorp.suiteapi.expression.annotations.Category;
import com.appiancorp.suiteapi.expression.annotations.Function;
import com.appiancorp.suiteapi.expression.annotations.Parameter;

@Category("category.name.ArrayFunctions")
public class SortingFunctions {

	@Function
	public String[] alphabeticalSort(@Parameter String[] textList) {
		try{
			Arrays.sort(textList);
			return textList;
		}catch (Exception e) {
			return null;
		}
        
	}

}
